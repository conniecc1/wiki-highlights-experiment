# Wiki Highlights Experiment

[Wiki Highlights](https://www.mediawiki.org/wiki/Wiki-Highlights) s a test project to validate or invalidate a hypothesis that if the global youth are offered automated, human-reviewed, visual articles as an alternative reading experience in third-party platforms, then we will increase their awareness and engagement of Wikimedia projects as readers and contributors.
